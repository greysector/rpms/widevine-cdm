%global debug_package %{nil}
%global __strip       /bin/true

# https://chromium.googlesource.com/chromiumos/overlays/chromiumos-overlay/+/refs/heads/main/chromeos-base/chromeos-lacros/
%global lacros_version 120.0.6098.0
%global a64_ver 4.10.2662.3
%global x64_ver 4.10.2830.0

%ifarch x86_64
%global warch x64
Version: %{x64_ver}
%else
%global warch arm64
Version: %{a64_ver}
%endif

Name: widevine-cdm
Summary: Widevine DRM browser plugin
Release: 1
License: Widevine
URL: https://www.widevine.com/solutions/widevine-drm
# https://hg.mozilla.org/mozilla-central/file/tip/toolkit/content/gmp-sources/widevinecdm.json
Source0: https://redirector.gvt1.com/edgedl/widevine-cdm/%{x64_ver}-linux-x64.zip
Source1: https://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/chromeos-lacros-arm64-squash-zstd-%{lacros_version}#/lacros-%{lacros_version}.squashfs
Source2: https://github.com/AsahiLinux/widevine-installer/raw/main/widevine_fixup.py
Patch0: %{name}-manifest-linux-arm64.patch
ExclusiveArch: aarch64 x86_64
%ifarch aarch64
BuildRequires: python3
BuildRequires: squashfs-tools
%endif
Requires: mozilla-filesystem
Enhances: chromium
Enhances: firefox
Enhances: qt5-qtwebengine

%description
Widevine’s DRM solution provides the capability to license, securely distribute
and protect playback of content on any consumer device. Content owners, multiple
service operators and digital media providers can utilize Widevine’s solutions
to ensure revenue generating services keep flowing to whatever device consumers
desire.

This package provides support for the following browsers:
Chromium
Firefox (firefox)
QT5 WebEngine (qt5-qtwebengine)

%prep
%ifarch x86_64
%setup -qc
mv LICENSE.txt LICENSE
%else
%setup -qcT
unsquashfs -q %{S:1}
%patch 0 -p1 -b .orig
cp -p squashfs-root/WidevineCdm/{LICENSE,manifest.json} ./
python3 %{S:2} -v squashfs-root/WidevineCdm/_platform_specific/cros_arm64/libwidevinecdm.so libwidevinecdm.so
%endif

%build

%install
mkdir -p %{buildroot}%{_libdir}/%{name}
install -p -m0755 libwidevinecdm.so %{buildroot}%{_libdir}/%{name}/
install -p -m0644 LICENSE manifest.json %{buildroot}%{_libdir}/%{name}/

# Install mozilla plugin
mkdir -p %{buildroot}%{_libdir}/mozilla/plugins/gmp-widevinecdm/system-installed
for f in libwidevinecdm.so LICENSE manifest.json ; do
  ln -s %{_libdir}/%{name}/${f} \
    %{buildroot}%{_libdir}/mozilla/plugins/gmp-widevinecdm/system-installed/
done

mkdir -p %{buildroot}%{_libdir}/firefox/defaults/pref
cat > %{buildroot}%{_libdir}/firefox/defaults/pref/widevinecdm.js << EOF
pref("media.eme.enabled", true);
pref("media.eme.encrypted-media-encryption-scheme.enabled", true);
pref("media.gmp-widevinecdm.autoupdate", false);
pref("media.gmp-widevinecdm.enabled", true);
pref("media.gmp-widevinecdm.version", "system-installed");
pref("media.gmp-widevinecdm.visible", true);
EOF

mkdir -p %{buildroot}%{_sysconfdir}/profile.d
cat > %{buildroot}%{_sysconfdir}/profile.d/widevinecdm.sh << EOF
MOZ_GMP_PATH="\${MOZ_GMP_PATH}\${MOZ_GMP_PATH:+:}%{_libdir}/mozilla/plugins/gmp-widevinecdm/system-installed"
export MOZ_GMP_PATH
EOF

# chromium plugin
mkdir -p %{buildroot}%{_libdir}/chromium-browser/WidevineCdm/_platform_specific/linux_%{warch}
ln -s ../../../../widevine-cdm/libwidevinecdm.so %{buildroot}%{_libdir}/chromium-browser/WidevineCdm/_platform_specific/linux_%{warch}
ln -s ../../widevine-cdm/manifest.json %{buildroot}%{_libdir}/chromium-browser/WidevineCdm

# qtwebengine plugin
mkdir -p %{buildroot}%{_libdir}/qt5/plugins/webengine
ln -s ../../../widevine-cdm/libwidevinecdm.so %{buildroot}%{_libdir}/qt5/plugins/webengine/

%files
%license LICENSE
%{_libdir}/%{name}
%dir %{_libdir}/chromium-browser
%dir %{_libdir}/chromium-browser/WidevineCdm
%{_libdir}/chromium-browser/WidevineCdm/manifest.json
%dir %{_libdir}/chromium-browser/WidevineCdm/_platform_specific
%dir %{_libdir}/chromium-browser/WidevineCdm/_platform_specific/linux_%{warch}
%{_libdir}/chromium-browser/WidevineCdm/_platform_specific/linux_%{warch}/libwidevinecdm.so
%dir %{_libdir}/firefox
%dir %{_libdir}/firefox/defaults
%dir %{_libdir}/firefox/defaults/pref
%{_libdir}/firefox/defaults/pref/widevinecdm.js
%{_libdir}/mozilla/plugins/gmp-widevinecdm
%dir %{_libdir}/qt5
%dir %{_libdir}/qt5/plugins
%dir %{_libdir}/qt5/plugins/webengine
%{_libdir}/qt5/plugins/webengine/libwidevinecdm.so
%{_sysconfdir}/profile.d/widevinecdm.sh

%changelog
* Fri Nov 22 2024 Dominik Mierzejewski <dominik@greysector.net> - 4.10.2830.0-1
- update to 4.10.2830.0 (x86_64)

* Sun Dec 31 2023 Dominik Mierzejewski <dominik@greysector.net> - 4.10.2710.0-1
- update to 4.10.2710.0 (x86_64) and 4.10.2662.3 (aarch64)
- use aarch64 build from LaCrOs based on AsahiLinux installer and fixup scripts

* Wed Jun 28 2023 Dominik Mierzejewski <dominik@greysector.net> - 4.10.2557.0-2
- add aarch64 support and drop armv7hl support
- drop support for obsolete chromium variants

* Wed Jan 04 2023 Dominik Mierzejewski <dominik@greysector.net> - 4.10.2557.0-1
- update to 4.10.2557.0

* Mon May 30 2022 Dominik Mierzejewski <rpm@greysector.net> - 4.10.2449.0-1
- update to 4.10.2449.0

* Fri Jan 14 2022 Dominik Mierzejewski <rpm@greysector.net> - 4.10.2391.0-1
- update to 4.10.2391.0

* Wed Oct 14 2020 Dominik Mierzejewski <rpm@greysector.net> - 4.10.1679.0-1
- update to 4.10.1679.0
- add ARM binary extracted from ChromeOS image

* Fri Jan 17 2020 Dominik Mierzejewski <rpm@greysector.net> - 4.10.1582.2-1
- update to 4.10.1582.2
- use correct paths for current chromium flavours
- sort file list alphabetically

* Tue Sep 03 2019 Dominik Mierzejewski <rpm@greysector.net> - 4.10.1440.19-2
- switch to a single package

* Tue Aug 06 2019 Dominik Mierzejewski <rpm@greysector.net> - 4.10.1440.19-1
- update to 4.10.1440.19
- add qtwebengine support
- add chromium-vaapi support
- simplify chromium support (alternatives dropped)
- disable stripping

* Wed Jan 09 2019 Dominik Mierzejewski <rpm@greysector.net> - 1.4.9.1088-1
- updated to 1.4.9.1088

* Fri Nov 03 2017 Dominik Mierzejewski <rpm@greysector.net> - 1.4.8.1008-1
- updated to 1.4.8.1008
- renamed main package to widevine-cdm
- add firefox support

* Thu Sep 14 2017 Dominik Mierzejewski <rpm@greysector.net> - 1.4.8.1004-2
- change the soft dependency to reflect the package which owns the Fedora
  provided stub
- bump alternatives priority to win over Fedora-provided stub
- fix alternatives uninstall script

* Wed Jun 28 2017 Dominik Mierzejewski <rpm@greysector.net> - 1.4.8.1004-1
- switch to proper upstream URL
- update to 1.4.8.1004
- use alternatives

* Mon Jun 12 2017 Dominik Mierzejewski <rpm@greysector.net> - 1.4.8.977-1
- update to 1.4.8.977

* Wed Sep 28 2016 Dominik Mierzejewski <rpm@greysector.net> - 1.4.8.893-1
- initial package
